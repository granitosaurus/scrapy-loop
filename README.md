# scrapy-loop

Module for looping scrapy crawlers.

Usage:

    from scrapyloop import ScrapyLoop
    ScrapyLoop().loop_crawl(MySpider)
